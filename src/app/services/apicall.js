const temp = 'https://suitmedia-backend.suitdev.com/api/ideas?page[number]=1&page[size]=10&append[]=small_image&append[]=medium_image&sort=-published_at'

const headers = {
  'accept': 'application/json',
};

export async function fetchData(url) {
    // const api_URL = `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${pageNumber}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${timeline}`
  try {
    const response = await fetch(url, {
      method: 'GET', // or 'POST', 'PUT', etc.
      headers: headers,
      mode: 'cors',  
    });
      return response.json()
  } catch (error) {
    console.log(error)
  }
};