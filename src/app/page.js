'use client'

import Image from 'next/image'
import NavBar from './components/navBar'
import Filterdown from './components/filterdown'
import Card from './components/card'
import { fetchData } from './services/apicall';
import { useState, useEffect } from 'react';
import Dropdown from './components/dropdown'
import Header from './components/header'
import Pagination from './components/pagination'

const API_URL = 'https://suitmedia-backend.suitdev.com/api/ideas'

export default function Home() {
  const storedPage = localStorage.getItem('currentPage');
  const storedSubsPerPage = localStorage.getItem('subsPerPage');
  const storedTimeline = localStorage.getItem('timeline');

  const [currentPage, setCurrentPage] = useState(storedPage ? parseInt(storedPage, 10) : 1);
  const [subsPerPage, setSubsPerPage] = useState(storedSubsPerPage ? parseInt(storedSubsPerPage, 10) : 10);
  const [timeline, setTimeline] = useState(storedTimeline || 'published_at');
  const [nextUrl,setNextUrl]=useState();
  const [prevUrl,setPrevUrl]=useState();
  const [currentlUrl, setCurrentUrl] = useState(`${API_URL}?page[number]=${currentPage}&page[size]=${subsPerPage}&append[]=small_image&append[]=medium_image&sort=${timeline}`);
  const [datas, setDatas] = useState([])
  const [total, setTotal] = useState()
// 
  const intiialLoad = async () => {
    const response = await fetchData(currentlUrl);
    setDatas(response.data);
    console.log("API Response:", response);
    setTotal(response.meta.total);
    setNextUrl(response.links.next !== null ? `${API_URL}?page[number]=${currentPage + 1}&page[size]=${subsPerPage}&append[]=small_image&append[]=medium_image&sort=${timeline}` : null);
    setPrevUrl(response.links.prev !== null ? `${API_URL}?page[number]=${currentPage - 1}&page[size]=${subsPerPage}&append[]=small_image&append[]=medium_image&sort=${timeline}` : null);
    console.log(currentlUrl);
    console.log(decodeURIComponent(response.links.next));
    console.log(decodeURIComponent(response.links.prev));
    }

  const changeSubs = (number) => {
    setSubsPerPage(number)
    setCurrentUrl(`${API_URL}?page[number]=${currentPage}&page[size]=${number}&append[]=small_image&append[]=medium_image&sort=${timeline}`)
    localStorage.setItem('subsPerPage', number.toString());
  }

  const changeTimeline = (timeline) => {
    setTimeline(timeline)
    setCurrentUrl(`${API_URL}?page[number]=${currentPage}&page[size]=${subsPerPage}&append[]=small_image&append[]=medium_image&sort=${timeline}`)
    localStorage.setItem('timeline', timeline);
  }
// 
  function nextPage() {
    setCurrentUrl(nextUrl)
    setCurrentPage(currentPage + 1)
    localStorage.setItem('currentPage', (currentPage+1).toString())
  }
// 
  function prevPage() {
    setCurrentUrl(prevUrl)
    setCurrentPage(currentPage - 1)
    localStorage.setItem('currentPage', (currentPage - 1).toString())
  }

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber)
    setCurrentUrl(`${API_URL}?page[number]=${pageNumber}&page[size]=${subsPerPage}&append[]=small_image&append[]=medium_image&sort=${timeline}`)
    localStorage.setItem('currentPage', pageNumber.toString())
  }

  useEffect(() => {
    intiialLoad();
  },[currentlUrl]);

  return (
    <main className="flex min-h-screen flex-col items-center">
      <NavBar/>
      <Header/>

      <div className='flex flex-row items-start mx-5 my-2'>
        <h2 className='mr-2 py-2 items-start'>{nextUrl !== null ? `Showing ${subsPerPage * currentPage} from ${total} items` : `Showing ${total} from ${total} items` }</h2>
        <div className='flex flex-row mr-4'>
          <p className='mr-3 py-2'>Show Content</p>
          <div>
            <Dropdown
            changeSubs = {changeSubs}
            subsPerPage = {subsPerPage}
            />
          </div>
        </div>
        <div className='flex flex-row'>
          <p className='mr-3 py-2'>Filter By</p>
          <div>
            <Filterdown
            changeTimeline= {changeTimeline}
            timeline={timeline}
            />
          </div>
        </div>
        
      </div>
      
      
      <div className='flex flex-wrap items-center justify-center mx-5'>
      {datas.map((data) =>
        <Card
        key={data.id}
        image = {data.medium_image.length > 0 ? data.medium_image[0].url : null}
        date = {data.published_at}  
        title = {data.title}
        />
      )}
      </div>

      <Pagination
          postsPerPage={subsPerPage}
          totalPosts={total}
          paginate={paginate}
          currentPage={currentPage}
          previous={prevPage}
          next={nextPage}
        />
    </main>
  )
}