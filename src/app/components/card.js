import Image from "next/image";
import darkpoke from '/public/darkpoke.svg'
import { format } from "date-fns";

const Card = ({image, date, title}) => {
    var formDate = new Date(date)
    const formattedDate = format(formDate, "dd MMMM yyyy" );
    return(
        <div className="cards flex flex-col drop-shadow-lg w-[200px] h-[250px] m-2 rounded-lg border shadow-lg bg-slate-100">
            <div className="h-1/2 w-full relative">
                <Image
                    className="rounded-t-lg"
                    alt="card image"
                    src={image || darkpoke}
                    quality={100}
                    fill
                    loading="lazy"
                    style={{objectFit:"cover"}}
                />
            </div>
            <div className="pt-2 px-2 text-sm text-gray-500">
                {formattedDate}
            </div>
            <div className="px-2 pb-2 text-base font-bold">
                {title}
            </div>
        </div>
    );
};

export default Card;