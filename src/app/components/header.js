import Image from "next/image";
import mountains from '/public/mountains.jpg'

const Header = () => {
    return(
        <div className="relative h-1/5 w-screen bg-gray-600">
            <Image
                src={mountains}
                alt='header'
                height={100}
                style={{objectFit:"cover"}}
            />
        </div>
    )
}

export default Header;