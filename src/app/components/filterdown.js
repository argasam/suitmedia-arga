import { useState } from "react";
import { AiOutlineCaretUp } from "react-icons/ai";
import { AiOutlineCaretDown } from "react-icons/ai";

const Filterdown = ({changeTimeline, timeline}) => {
    const [isOpen, setIsOpen] = useState(false);

    const handleClick = (timeline) => {
        setIsOpen(false);
        changeTimeline(timeline);
    };

   return(
    <div className="relative flex flex-col items-center w-[120px] h-[50px]   rounded-lg">
        <button className="bg-white px-3 py-1 w-full flex relative  
        items-center justify-between text-sm rounded-full border-black border-2" 
        onClick={() => setIsOpen((prev) => !prev)}>
        {timeline === "published_at" ? "Newest" : "Oldest"}
        {!isOpen 

            ? <AiOutlineCaretDown className="h-8"/>
            : <AiOutlineCaretUp className="h-8"/>

        }
        </button>
        {isOpen && (
            <div className="bg-white fixed top-50 m-12 flex flex-col items-center w-[120px] rounded-2xl z-50">
                <h3 onClick={() => handleClick("published_at")} className="cursor-pointer hover:bg-gray-200 p-1 rounded">Newest</h3>
                <h3 onClick={() => handleClick("-published_at")} className="cursor-pointer hover:bg-gray-200 p-1 rounded">Oldest</h3>
            </div>
           )}
    
    </div>

   )
};

export default Filterdown   