import { useState } from "react";
import { AiOutlineCaretUp } from "react-icons/ai";
import { AiOutlineCaretDown } from "react-icons/ai";

const Dropdown = ({changeSubs, subsPerPage }) => {
    const [isOpen, setIsOpen] = useState(false);

    const handleClick = (number) => {
        setIsOpen(false);
        changeSubs(number)
    };

   return(
    <div className="relative flex flex-col items-center w-[70px] h-[50px] rounded-lg mr-2">
        <button className="bg-white px-3 py-1 w-full flex 
        items-center justify-between text-sm rounded-full border-black border-2" 
        onClick={() => setIsOpen((prev) => !prev)}>
        {subsPerPage}
        {!isOpen 

            ? <AiOutlineCaretDown className="h-8"/>
            : <AiOutlineCaretUp className="h-8"/>

        }
        </button>
        {isOpen && (
            <div className="bg-white fixed top-50 m-12 p-2 flex flex-col items-center w-[70px] rounded-2xl z-50">
                <h3 onClick={() => handleClick(10)} className="cursor-pointer hover:bg-gray-200 p-1 rounded">10</h3>
                <h3 onClick={() => handleClick(30)} className="cursor-pointer hover:bg-gray-200 p-1 rounded">30</h3>
                <h3 onClick={() => handleClick(50)} className="cursor-pointer hover:bg-gray-200 p-1 rounded">50</h3>
            </div>
           )}
    
    </div>
   );
};

export default Dropdown