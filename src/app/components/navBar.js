import Link from "next/link";
import suitwhite from '/public/suitwhite.png';
import Image from "next/image";

const NavBar = () => {
    return(
        <div className='TopBar flex flex-row w-screen justify-between py-6 bg-orange-suit px-36'>
            <Image
                priority
                className="mx-2"
                src={suitwhite}
                alt="logo"
                height={50}
            />
            <div className="flex flex-row justify-end content-between">
                <button className="mr-5 text-white">Work</button>
                <button className="mr-5 text-white">About</button>
                <button className="mr-5 text-white" >Services</button>
                <button className="mr-5 text-white">Ideas</button>
                <button className="mr-5 text-white">Careers</button>
                <button className="mr-5 text-white">Contact</button>
            </div>
        </div>
    );
};

export default NavBar;